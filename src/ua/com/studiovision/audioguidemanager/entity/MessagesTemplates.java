package ua.com.studiovision.audioguidemanager.entity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author justwatermelon
 */
public class MessagesTemplates {
    ArrayList<HashMap<String,String>> messages;

    public MessagesTemplates(ArrayList<HashMap<String, String>> messages) {
        this.messages = messages;
    }

    public ArrayList<HashMap<String, String>> getMessages() {
        return messages;
    }
}
