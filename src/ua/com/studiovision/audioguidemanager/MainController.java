package ua.com.studiovision.audioguidemanager;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.controlsfx.dialog.Dialogs;
import ua.com.studiovision.audioguidemanager.entity.Client;
import ua.com.studiovision.audioguidemanager.entity.DeviceListItem;
import ua.com.studiovision.audioguidemanager.handlers.UpdateHandler;
import ua.com.studiovision.audioguidemanager.networking.NetworkProtocol;
import ua.com.studiovision.audioguidemanager.networking.Server;
import ua.com.studiovision.audioguidemanager.networking.WorkerRunnable;
import ua.com.studiovision.audioguidemanager.requests.DataToReceive;
import ua.com.studiovision.audioguidemanager.requests.SyncRequest;
import ua.com.studiovision.audioguidemanager.tree.PathItem;
import ua.com.studiovision.audioguidemanager.tree.PathTreeItem;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainController {
    public ListView<DeviceListItem> devices;
    public TitledPane audioDirectories;
    public TreeView<PathItem> audioFiles;
    public ProgressBar syncProgressBar;
    public Button syncButton;
    public MenuItem settings;
    public Accordion rootAccordion;
    public Button audioListRequest;
    public Label syncText;
    public HBox statusBox;
    public Label actionText;
    public ListView<String> audioFilesOnDevice;
    public TitledPane audioFilesOnDevicePane;
    public MenuItem newPlaylist;
    public MenuItem usbSync;
    public MenuItem guideManagement;
    public MenuItem exportPlaylist;
    public MenuItem messagesEditor;
    private Server server;
    private static final String KEY = "key";
    public static final String PLAYLIST = "plst";

    private Stage stage;
    public static ObservableList<DeviceListItem> connectedDevicesModel;
    public static ObservableList<String> audioFilesOnDeviceModel;
    public static HashMap<Path, String> serverAudioHashList = new HashMap<>();
    public static HashMap<Client, ArrayList<Path>> responseHashMap = new HashMap<>();
    public static int sentFiles;
    public static int sendTotal;
    public static double updateValue;
    public static UpdateHandler updateHandler;
    public static ArrayList<String> failureSyncClients = new ArrayList<>();


    ApplicationProperties properties = ApplicationProperties.getInstance();

    @FXML
    void initialize() {
        setAudioDirectories();
        connectedDevicesModel = FXCollections.observableArrayList();
        audioFilesOnDeviceModel = FXCollections.observableArrayList();
        devices.setItems(connectedDevicesModel);
        audioFilesOnDevice.setItems(audioFilesOnDeviceModel);
        updateHandler = new UpdateHandler() {
            @Override
            public synchronized void update() {
                sentFiles++;
                Platform.runLater(() -> {
                    syncProgressBar.setProgress(syncProgressBar.getProgress() + updateValue);
                    syncText.setText(sentFiles + " из " + sendTotal + " файлов отправлено");
                });
            }

            @Override
            public void hide() {
                Platform.runLater(() -> {
                    statusBox.setVisible(false);
                    audioListRequest.setDisable(false);
                });
            }

            @Override
            public synchronized void incrementAudioLists() {
                Server.audioListResponses++;
            }

            @Override
            public void showButtons() {
                Platform.runLater(() -> {
                    syncButton.setDisable(false);
                    audioListRequest.setDisable(false);
                });
            }

            @Override
            public void refreshDevices() {
                Platform.runLater(() -> {
                    devices.setItems(null);
                    devices.setItems(connectedDevicesModel);
                    connectedDevicesModel.sort(Comparator.comparing(DeviceListItem::getBoundedName));
                });
            }

            @Override
            public void refreshServerAudio() {
                setAudioDirectories();
            }

            @Override
            public void progressBarRepaint() {
                Platform.runLater(() -> syncProgressBar.setStyle("-fx-accent: orange;"));
            }

            @Override
            public void showFailureTransferClients() {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.setLength(0);
                failureSyncClients.forEach(client -> stringBuilder.append("\n").append(client));
                Platform.runLater(() -> {
                    syncText.setText("Неполная синхронизация!");
                    Dialogs.create()
                            .owner(stage)
                            .title("Внимание! Повторите синхронизацию")
                            .message("Сбой передачи файлов клиентам: " + stringBuilder.toString())
                            .showWarning();
                });
            }
        };
        devices.setOnMouseClicked(event -> {
            try {
                if (devices.getSelectionModel().getSelectedItem().getClient().getAudios() != null && rootAccordion.getExpandedPane().equals(audioDirectories)) {
                    rootAccordion.setExpandedPane(audioFilesOnDevicePane);
                }
                audioFilesOnDevice.getItems().clear();
                if (devices.getSelectionModel().getSelectedItem().getClient().getAudios() != null) {
                    devices.getSelectionModel().getSelectedItem().getClient().getAudios().forEach((fileName, Hash) -> audioFilesOnDevice.getItems().add(fileName));
                }
            } catch (Exception e) {
                Logger.getGlobal().info("Empty selection");
            }
        });
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public static synchronized int getSentFiles() {
        return sentFiles;
    }

    public void onSettingsClick(ActionEvent actionEvent) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("SettingsLayout.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Настройки");
            stage.setScene(new Scene(root));
            SettingsController controller = loader.getController();
            controller.setStage(stage);
            controller.setMainController(this);
            stage.initStyle(StageStyle.UTILITY);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(this.stage.getScene().getWindow());
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void setAudioDirectories() {
        audioDirectories.setContent(null);
        audioFiles = new TreeView<>();
        audioFiles.setShowRoot(false);
        audioFiles.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        audioFiles.setRoot(PathTreeItem.createNode(new PathItem(Paths.get(properties.getRootDirectory())), false));
        if (audioFiles.getTreeItem(0) != null) {
            audioFiles.getTreeItem(0).setExpanded(true);
        }

        Platform.runLater(() -> {
            Logger.getGlobal().info("Mp3 to mp3s encryption started...");
            EncryptService encryptService = encryptAllMP3();
            encryptService.setOnSucceeded(event -> Platform.runLater(() -> {
                Logger.getGlobal().info("Encryption finished.");
                audioDirectories.setContent(audioFiles);
                rootAccordion.setExpandedPane(audioDirectories);
            }));
        });
    }

    public void startNetworking(int port) {
        server = new Server(port);
        new Thread(server).start();
        stage.setOnCloseRequest(event -> server.stop());
    }

    public void onGetAudioListClick(ActionEvent actionEvent) {
        if (Server.clients.values().size() != 0) {
            Server.audioListResponses = 0;
            actionText.setText("Выполняеся запрос аудиозаписей");
            statusBox.setVisible(true);
            audioListRequest.setDisable(true);
            syncButton.setDisable(true);
            for (Client client : Server.clients.values()) {
                //SEND AUDIO REQUEST
                synchronized (this) {
                    String request = WorkerRunnable.getGson().toJson(new SyncRequest(null));
                    try {
                        if (!client.workerRunnable.getTimeoutThread().getState().equals(Thread.State.NEW)) {
                            client.workerRunnable.setTimeoutThread(client.workerRunnable.threadCreate(WorkerRunnable.REQUEST_TIMEOUT));
                        }
                        client.workerRunnable.getTimeoutThread().start();
                        client.workerRunnable.sendMessage(NetworkProtocol.SYNC_REQUEST, request);
                    } catch (IOException e) {
                        Logger.getGlobal().log(Level.SEVERE, "IO error...", e);
                    }
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    public void onSyncButtonClick(ActionEvent actionEvent) {
        responseHashMap.clear();
        failureSyncClients.clear();
        syncText.setText("");
        syncProgressBar.setStyle("-fx-accent: #0088DD;");
        syncProgressBar.setProgress(0);

        boolean found = false;
        for (Client client : Server.clients.values()) {
            if (client.getAudios() != null) {
                found = true;
                break;
            }
        }

        if (!found) {
            Dialogs.create()
                    .owner(stage)
                    .title("Внимание!")
                    .message("Не найдено аудиозаписей на устройствах. Пожалуйста, выполните запрос.")
                    .showWarning();
        } else {
            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    buildServerAudiosHash();
                    //calculate diff & build response list
                    for (Client client : Server.clients.values()) {
                        responseHashMap.put(client, calculateDifference(client));
                    }
                    Logger.getGlobal().info("Server audio hash & difference calculated");
                    //data to receive
                    updateValue = 1.0 / MainController.sendTotal;
                    responseHashMap.forEach((client, responses) -> {
                        Thread thread = new Thread(() -> {
                            try {
                                if (!client.workerRunnable.getTimeoutThread().getState().equals(Thread.State.NEW)) {
                                    client.workerRunnable.setTimeoutThread(client.workerRunnable.threadCreate(WorkerRunnable.REQUEST_TIMEOUT));
                                }
                                client.workerRunnable.getTimeoutThread().start();
                                Logger.getGlobal().info("TO SEND:" + responses.size() + " CLIENT: " + client.getName());
                                client.workerRunnable.sendMessage(NetworkProtocol.DATA_TO_RECEIVE, WorkerRunnable.getGson().toJson(new DataToReceive(responses.size(), 0)));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                        thread.start();
                        System.out.println("Thread for client: " + client.getName() + " started");
                        try {
                            synchronized (WorkerRunnable.lock) {
                                WorkerRunnable.lock.wait();
                                System.out.println("Thread for client: " + client.getName() + " finished");
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
                    return null;
                }
            };
            syncButton.setDisable(true);
            audioListRequest.setDisable(true);
            actionText.setText("Выполняется процесс синхронзации");
            statusBox.setVisible(true);
            Logger.getGlobal().info("Start building server hash and difference");
            new Thread(task).start();
        }
    }

    private ArrayList<Path> calculateDifference(Client client) {
        ArrayList<Path> responses = new ArrayList<>();
        StringBuilder comparePath = new StringBuilder();
        if (client.getType().equals("tourist")) {
            serverAudioHashList.forEach((current, value) -> {
                comparePath.setLength(0);
                comparePath.append(current.toString().replace("\\", "/")); //convert windows path format to *nix
                if (client.getAudios() != null && client.getAudios().containsKey(comparePath.toString()) && client.getAudios().get(comparePath.toString()).equals(value)) {
                } else if (!KEY.equals(FilenameUtils.getExtension(current.getFileName().toString())) && !PLAYLIST.equals(FilenameUtils.getExtension(current.getFileName().toString()))) {
                    sendTotal++;
                    responses.add(Paths.get(comparePath.toString()));
                }
            });
        } else {
            serverAudioHashList.forEach((current, value) -> {
                comparePath.setLength(0);
                comparePath.append(current.toString().replace("\\", "/")); //convert windows path format to *nix
                if (client.getAudios() != null && client.getAudios().containsKey(comparePath.toString()) && client.getAudios().get(comparePath.toString()).equals(value)) {
                } else {
                    sendTotal++;
                    responses.add(Paths.get(comparePath.toString()));
                }
            });
        }
        Logger.getGlobal().info("Audio difference for client " + client.getName() + " created.");
        return responses;
    }

    public void buildServerAudiosHash() {
        sendTotal = 0;
        sentFiles = 0;
        try {
            Files.walk(Paths.get(properties.getRootDirectory())).forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    try {
                        serverAudioHashList.put(Paths.get(filePath.toString().replace(properties.getRootDirectory(), "")), EncryptUtil.getMD5(filePath.toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private EncryptService encryptAllMP3() {
        EncryptService encryptService = new EncryptService();
        Dialogs.create().title("Пожалуйста, подождите").message("Выполняется шифрование файлов . . .")
                .showWorkerProgress(encryptService);
        encryptService.start();
        return encryptService;
    }

    public void onNewPlaylistClick(ActionEvent actionEvent) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("PlaylistLayout.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Создать новый плейлист");
            stage.setScene(new Scene(root));
            PlaylistController controller = loader.getController();
            controller.setStage(stage);
            controller.setUpdateHandler(updateHandler);
            stage.initStyle(StageStyle.UTILITY);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(this.stage.getScene().getWindow());
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onUsbSyncClick(ActionEvent actionEvent) {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        final File selectedDirectory = directoryChooser.showDialog(stage);
        if (selectedDirectory != null) {
            CopyingService service = new CopyingService(selectedDirectory, new File(ApplicationProperties.getInstance().getRootDirectory()), FileUtils.sizeOfDirectory(selectedDirectory));
            Dialogs.create().
                    masthead("Синхронизация с USB . . .").
                    showWorkerProgress(service);
            service.start();
            service.setOnSucceeded(event -> setAudioDirectories());
        }
    }

    public void onGuideManagementClick(ActionEvent actionEvent) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("GuideRegisterLayout.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Управление гидами");
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UTILITY);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(this.stage.getScene().getWindow());
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onExportPlaylistClick(ActionEvent actionEvent) {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        final File selectedDirectory = directoryChooser.showDialog(stage);
        if (selectedDirectory != null) {
            File playListsDirectory = new File(ApplicationProperties.getInstance().getRootDirectory().concat("\\").concat("play-lists"));
            CopyingService service = new CopyingService(playListsDirectory, selectedDirectory, FileUtils.sizeOfDirectory(playListsDirectory));
            service.start();
            service.setOnSucceeded(event -> Dialogs.create().title("Экспорт плейлистов")
                    .message("Плейлисты успешно экспортированны")
                    .showInformation());
            service.setOnFailed(event -> Dialogs.create().title("Экспорт плейлистов")
                    .message("Ошибка во время экспорта")
                    .showError());
        }
    }

    public void onHelpClicked(ActionEvent actionEvent) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("HelpLayout.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Инструкция");
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.DECORATED);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onMessagesEditorClick(ActionEvent actionEvent) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("MessagesLayout.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Редактор шаблонов");
            stage.setScene(new Scene(root));
            MessagesController controller = loader.getController();
            stage.setOnCloseRequest(event -> controller.saveToFile());
            stage.initStyle(StageStyle.UTILITY);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(this.stage.getScene().getWindow());
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}