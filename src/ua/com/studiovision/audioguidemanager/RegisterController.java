package ua.com.studiovision.audioguidemanager;

import com.google.gson.Gson;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import jfxtras.internal.scene.control.skin.CalendarPickerControlSkin;
import jfxtras.scene.control.CalendarPicker;
import org.apache.commons.io.IOUtils;
import org.controlsfx.dialog.Dialogs;
import ua.com.studiovision.audioguidemanager.entity.GuideListItem;
import ua.com.studiovision.audioguidemanager.entity.RegisteredGuides;
import ua.com.studiovision.audioguidemanager.entity.SavedNames;
import ua.com.studiovision.audioguidemanager.networking.HttpWorker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justwatermelon
 */
public class RegisterController {
    public ListView<GuideListItem> guideList;
    public Accordion timeSetAccordion;
    public TitledPane timeFromPane;
    public Slider hourFromSlider;
    public Slider minuteFromSlider;
    public TitledPane timeToPane;
    public Slider hourToSlider;
    public Slider minuteToSlider;
    public Label hourFrom;
    public Label minuteFrom;
    public Label hourTo;
    public Label minuteTo;
    public AnchorPane timeContainer;
    public Button doActionButton;
    public Pane calendarHolder;
    public CalendarPicker calendarPicker;

    private ObservableList<GuideListItem> guideListModel = FXCollections.observableArrayList();

    DecimalFormat decimalFormat = new DecimalFormat("00");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private static final Gson gson = new Gson();

    @FXML
    void initialize() {
        timeSetAccordion.setExpandedPane(timeFromPane);
        hourFromSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            hourFrom.textProperty().setValue(decimalFormat.format((long) hourFromSlider.getValue()));
        });
        minuteFromSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            minuteFrom.textProperty().setValue(decimalFormat.format((long) minuteFromSlider.getValue()));
        });
        hourToSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            hourTo.textProperty().setValue(decimalFormat.format((long) hourToSlider.getValue()));
        });
        minuteToSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            minuteTo.textProperty().setValue(decimalFormat.format((long) minuteToSlider.getValue()));
        });
        prepareGuideList();
        guideList.setItems(guideListModel);
        if (guideList.getSelectionModel().getSelectedItem() == null) {
            timeContainer.setDisable(true);
        }
        guideList.setOnMouseClicked(event -> {
            GuideListItem selected = guideList.getSelectionModel().getSelectedItem();
            if (guideList.getSelectionModel().getSelectedIndex()>=0) {
                timeContainer.setDisable(false);
                setSliders(selected.getTimeFrom(), selected.getTimeTo());
                setSelectedDates(selected.getCalendars());
                GregorianCalendar calendar = new GregorianCalendar();
                try {
                    calendar.setTime(simpleDateFormat.parse(simpleDateFormat.format(new Date())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                calendarPicker.setDisplayedCalendar(calendar);
                if (!selected.isRegister()) {
                    doActionButton.setText("Изменить");
                } else {
                    doActionButton.setText("Зарегистрировать");
                }
            }
        });
        calendarPicker = new CalendarPicker();
        calendarPicker.setMode(CalendarPicker.Mode.MULTIPLE);
        calendarPicker.setSkin(calendarPicker.createDefaultSkin());
        CalendarPickerControlSkin skin = (CalendarPickerControlSkin) calendarPicker.getSkin();
        skin.setShowWeeknumbers(CalendarPickerControlSkin.ShowWeeknumbers.NO);
        calendarHolder.getChildren().add(calendarPicker);
    }

    private void setSliders(String timeFrom, String timeTo) {
        if (timeFrom != null && timeTo != null) {
            String[] fromArray = timeFrom.split(":");
            String[] toArray = timeTo.split(":");
            hourFromSlider.setValue(Double.parseDouble(fromArray[0]));
            minuteFromSlider.setValue(Double.parseDouble(fromArray[1]));
            hourToSlider.setValue(Double.parseDouble(toArray[0]));
            minuteToSlider.setValue(Double.parseDouble(toArray[1]));
        } else {
            hourFromSlider.setValue(0);
            minuteFromSlider.setValue(0);
            hourToSlider.setValue(23);
            minuteToSlider.setValue(59);
        }
    }

    private void setSelectedDates(HashSet<String> dates){
        dates.forEach(date -> {
            try {
                GregorianCalendar toSet = new GregorianCalendar();
                toSet.setTime(simpleDateFormat.parse(date));
                calendarPicker.calendars().add(toSet);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
    }

    private void prepareGuideList() {
        SavedNames guides = gson.fromJson(loadSavedNames(), SavedNames.class);
        RegisteredGuides registeredGuides = gson.fromJson(loadRegisteredGuides(), RegisteredGuides.class);
        if (registeredGuides != null) {
            System.out.println(registeredGuides.getRegisteredGuides());
            registeredGuides.getRegisteredGuides().forEach((imei, listItem) -> guideListModel.add(listItem));
            guides.getNames().forEach((imei, name) -> {
                if (!registeredGuides.getRegisteredGuides().containsKey(imei)) {
                    guideListModel.add(new GuideListItem(name, imei, null, null, null, true));
                }
            });
        } else {
            guides.getNames().forEach((imei, name) -> guideListModel.add(new GuideListItem(name, imei, null, null, null, true)));
        }
    }

    private void saveRegisteredGuides() {
        GuideListItem selected = guideList.getSelectionModel().getSelectedItem();
        if (selected != null) {
            selected.setTimeFrom(hourFrom.getText() + ":" + minuteFrom.getText() + ":" + "00");
            selected.setTimeTo(hourTo.getText() + ":" + minuteTo.getText() + ":" + "00");
            selected.setTimezone("+" + getTimeZoneOffset());
            selected.getCalendars().clear();
            calendarPicker.calendars().forEach(date -> selected.getCalendars().add(simpleDateFormat.format(date.getTime())));
            selected.getCalendars().forEach(System.out::println);
            String[] result = HttpWorker.performRequest(selected);
            Dialogs dialog = Dialogs.create().message(result[1]).title("Результат запроса");
            switch (Integer.parseInt(result[0])) {
                case 0:
                    dialog.showInformation();
                    selected.setRegister(false);
                    break;
                case 1:
                    dialog.showInformation();
                    break;
                case 4:
                    dialog.showWarning();
                    selected.setRegister(false);
                    doActionButton.setText("Изменить");
                    break;
                default:
                    dialog.showError();
                    return;
            }
        }

        HashMap<String, GuideListItem> toSave = new HashMap<>();
        guideListModel.forEach((listItem) -> toSave.put(listItem.getImei(), listItem));
        String toSaveJson = new Gson().toJson(new RegisteredGuides(toSave));
        File registeredGuidesFile = new File("registered_guides.agm");
        if (!registeredGuidesFile.getAbsoluteFile().exists()) {
            try {
                Logger.getGlobal().info("Registered guides file creation status: " + registeredGuidesFile.createNewFile());
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE,e.getMessage(),e);
            }
        }
        try {
            IOUtils.write(toSaveJson.getBytes(), new FileOutputStream(registeredGuidesFile));
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, "IOException while saving names...",e);
        }
        Logger.getGlobal().info("Names saved...");
    }

    private String loadSavedNames() {
        File file = new File("saved_guides.agm");
        FileInputStream fis;
        String result = null;
        if (file.getAbsoluteFile().exists()) {
            try {
                fis = new FileInputStream(file);
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                fis.close();
                result = new String(data, "UTF-8");
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE, "IOException while loading names...",e);
            }
            Logger.getGlobal().info("Guides loaded...");
        }
        return result;
    }

    private String loadRegisteredGuides() {
        File file = new File("registered_guides.agm");
        FileInputStream fis;
        String result = null;
        if (file.getAbsoluteFile().exists()) {
            try {
                fis = new FileInputStream(file);
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                fis.close();
                result = new String(data, "UTF-8");
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE,"IOException while loading names...",e);
            }
            Logger.getGlobal().info("Guides loaded...");
        }
        return result;
    }

    public static String getTimeZoneOffset() {
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetFromUtc = tz.getOffset(now.getTime()) / 3600000;
        return Integer.toString(offsetFromUtc);
    }

    public void onActionClick(ActionEvent actionEvent) {
        saveRegisteredGuides();
    }
}