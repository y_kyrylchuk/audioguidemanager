package ua.com.studiovision.audioguidemanager;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;
import ua.com.studiovision.audioguidemanager.handlers.UpdateHandler;
import ua.com.studiovision.audioguidemanager.tree.PathItem;
import ua.com.studiovision.audioguidemanager.tree.PathTreeItem;
import ua.com.studiovision.audioguidemanager.tree.PlaylistItem;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author justwatermelon
 */
public class PlaylistController {
    public TextField playlistName;
    public TreeView<PathItem> availableObjectsTreeView;
    public ListView<PlaylistItem> playlist;
    public Button addToPlaylist;
    public Button removeFromPlaylist;
    public Button cancelButton;
    public Button createButton;
    public Button upButton;
    public Button downButton;
    public Button addAllButton;
    private Stage stage;
    private static final String EXTENSION = ".plst";
    private static final String PLAYLIST_NAME_PATTERN = "[\\p{IsAlphabetic}\\d]*";

    private UpdateHandler updateHandler;
    public static final String replacePattern = ApplicationProperties.getInstance().getRootDirectory() + "\\" + "audio" + "\\";
    private ArrayList<String> playLists = new ArrayList<>();
    private ObservableList<PlaylistItem> playlistAudioModel = FXCollections.observableArrayList();

    @FXML
    void initialize() {
        addToPlaylist.setGraphic(new ImageView(PlaylistController.class.getResource("/resources/add.png").toString()));
        removeFromPlaylist.setGraphic(new ImageView(PlaylistController.class.getResource("/resources/remove.png").toString()));
        addAllButton.setGraphic(new ImageView(PlaylistController.class.getResource("/resources/addAll.png").toString()));
        upButton.setGraphic(new ImageView(PlaylistController.class.getResource("/resources/up.png").toString()));
        downButton.setGraphic(new ImageView(PlaylistController.class.getResource("/resources/down.png").toString()));
        availableObjectsTreeView.setRoot(PathTreeItem.createNode(new PathItem(Paths.get(ApplicationProperties.getInstance().getRootDirectory() + "\\" + "audio")), true));
        expandTreeView(availableObjectsTreeView.getRoot());
        File playlistFolder = new File(ApplicationProperties.getInstance().getRootDirectory() + "\\" + "play-lists");
        if (!playlistFolder.getAbsoluteFile().exists()) {
            Dialogs.create().title("Внимание!")
                    .message("Каталог плейлистов не найден.\n" +
                            "Будет создан новый каталог.")
                    .showWarning();
            try {
                Files.createDirectory(playlistFolder.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File[] files = playlistFolder.listFiles();
        System.out.println(files.length);
        for (int i = 0; i < files.length; i++) {
            playLists.add(files[i].getName());
        }
        playlist.setItems(playlistAudioModel);
        playlist.getParent().setOnMouseClicked(event -> {
            playlist.getSelectionModel().select(-1);
            availableObjectsTreeView.getSelectionModel().select(-1);
        });
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setUpdateHandler(UpdateHandler updateHandler) {
        this.updateHandler = updateHandler;
    }

    private void expandTreeView(TreeItem<PathItem> item) {
        if (item != null && !item.isLeaf()) {
            item.setExpanded(true);
            item.getChildren().forEach(this::expandTreeView);
        }
    }

    public void onCreateButtonClick(ActionEvent actionEvent) {

        if (playlistName.getText().length() == 0 || playlistName.getText().trim().length() == 0) {
            Dialogs.create().
                    title("Не введено имя!").
                    message("Пожалуйста, введите имя плейлиста.").
                    showWarning();
            return;
        }

        if (playlist.getItems().size() == 0) {
            Dialogs.create().
                    title("Пустой плейлист!").
                    message("Пожалуйста, заполните плейлист.").
                    showWarning();
            return;
        }
        Pattern pattern = Pattern.compile(PLAYLIST_NAME_PATTERN);
        Matcher matcher = pattern.matcher(playlistName.getText().trim());
        if (playlistName.getText().equals(" ") && playlistName.getText().length()<=0) {
            Dialogs.create().
                    title("Некорректное имя плейлиста!").
                    message("Пожалуйста, измените имя плейлиста.").
                    showWarning();
            return;
        }

        if (playLists.stream().anyMatch(playlistFile -> playlistFile.equals(playlistName.getText() + EXTENSION))) {
            Dialogs.create().
                    title("Такой плейлист уже существует!").
                    message("Пожалуйста, измените имя плейлиста.").
                    showWarning();
            return;
        }

        File file = new File(ApplicationProperties.getInstance().getRootDirectory() + File.separator + "play-lists" + File.separator + playlistName.getText() + EXTENSION);
        if (!file.getAbsoluteFile().exists()) {
            try {
                Logger.getGlobal().info("Playlist file creation state: " + file.createNewFile());
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE,"IOException was thrown...",e);
            }
        }
        try {
            PrintWriter printWriter = new PrintWriter(file,"UTF-8");
            playlist.getItems().forEach((playlistItem) -> {
                String toAdd = playlistItem.getPathItem().getPath().toString().substring(replacePattern.length()).replace("\\", "/");
                printWriter.write(playlistItem.getPathItem().getPath().getFileName() + ":" + toAdd + "\n");
            });
            printWriter.flush();
            printWriter.close();
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE,"No such file: " + file.getName());
        }

        stage.close();
        updateHandler.refreshServerAudio();
    }

    public void onAddButtonClick(ActionEvent actionEvent) {
        PathItem selectedItem = availableObjectsTreeView.getSelectionModel().getSelectedItem().getValue();
        String firstStep = selectedItem.getPath().toString().substring(replacePattern.length());
        if (availableObjectsTreeView.getSelectionModel().getSelectedItem().isLeaf()) {
            String playlistOut = firstStep.replace("\\", " - ");
            PlaylistItem playlistItem = new PlaylistItem(selectedItem, playlistOut);
            if (playlistAudioModel.stream().anyMatch(item -> item.toString().equals(playlistItem.toString()))) {
                Dialogs.create().
                        title("Внимание!").
                        message("Этот объект уже добавлен.").
                        showWarning();
            } else {
                playlistAudioModel.add(playlistItem);
            }
        }
    }

    public void onRemoveButtonClick(ActionEvent actionEvent) {
        System.out.println(playlist.getSelectionModel().getSelectedItem() + "=" + playlist.getSelectionModel().getSelectedIndex());
        if (playlist.getSelectionModel().getSelectedItem() != null) {
            if (playlistAudioModel.size() == 1) {
                playlistAudioModel.clear();
            } else {
                playlistAudioModel.remove(playlist.getSelectionModel().getSelectedItem());
            }
            playlist.fireEvent(new ListView.EditEvent<>(playlist, ListView.editCommitEvent(), null, -1));
        }
    }

    public void onCancelButtonClick(ActionEvent actionEvent) {
        stage.close();
        updateHandler.refreshServerAudio();
    }

    public void onUpClick(ActionEvent actionEvent) {
        if (playlistAudioModel.size() > 1) {
            int selectedIndex = playlist.getSelectionModel().getSelectedIndex();
            if (selectedIndex != -1 && selectedIndex != 0) {
                PlaylistItem selectedItem = playlist.getItems().get(selectedIndex);
                PlaylistItem tempItem = playlistAudioModel.get(selectedIndex - 1);
                playlistAudioModel.set(selectedIndex - 1, selectedItem);
                playlistAudioModel.set(selectedIndex, tempItem);
                playlist.fireEvent(new ListView.EditEvent<>(playlist, ListView.editCommitEvent(), null, -1));
                playlist.getSelectionModel().select(selectedIndex - 1);
            }
        }
    }

    public void onDownClick(ActionEvent actionEvent) {
        if (playlistAudioModel.size() > 1) {
            int selectedIndex = playlist.getSelectionModel().getSelectedIndex();
            if (selectedIndex != -1 && selectedIndex != playlistAudioModel.size() - 1) {
                PlaylistItem selectedItem = playlist.getItems().get(selectedIndex);
                PlaylistItem tempItem = playlistAudioModel.get(selectedIndex + 1);
                playlistAudioModel.set(selectedIndex + 1, selectedItem);
                playlistAudioModel.set(selectedIndex, tempItem);
                playlist.fireEvent(new ListView.EditEvent<>(playlist, ListView.editCommitEvent(), null, -1));
                playlist.getSelectionModel().select(selectedIndex + 1);
            }
        }
    }

    public void onAddAllButtonClick(ActionEvent actionEvent) {
        treeWalk(availableObjectsTreeView.getRoot());
    }

    private void treeWalk(TreeItem<PathItem> treeItem) {
        if (treeItem != null && !treeItem.isLeaf()) {
            treeItem.setExpanded(true);
            treeItem.getChildren().forEach(this::treeWalk);
        } else {
            PathItem currentItem = treeItem.getValue();
            String playlistOut = currentItem.getPath().toString().substring(replacePattern.length()).replace("\\", " - ");
            PlaylistItem playlistItem = new PlaylistItem(currentItem, playlistOut);
            if (!playlistAudioModel.stream().anyMatch(obj -> obj.getPlaylistOutput().equals(playlistItem.getPlaylistOutput()))) {
                playlistAudioModel.add(playlistItem);
            }
        }
    }
}