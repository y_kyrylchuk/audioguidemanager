package ua.com.studiovision.audioguidemanager.entity;

import java.util.HashMap;

/**
 * @author justwatermelon
 */
public class SavedNames {
    private HashMap<String,String> names;

    public SavedNames(HashMap<String, String> names) {
        this.names = names;
    }

    public HashMap<String, String> getNames() {
        return names;
    }
}
