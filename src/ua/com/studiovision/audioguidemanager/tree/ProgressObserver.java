package ua.com.studiovision.audioguidemanager.tree;

import javafx.fxml.FXMLLoader;
import ua.com.studiovision.audioguidemanager.MainController;

/**
 * @author justwatermelon
 */
public class ProgressObserver implements Runnable {

    MainController controller;

    public ProgressObserver() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MainLayout.fxml"));
        this.controller = fxmlLoader.getController();
    }

    @Override
    public void run() {
        synchronized (this) {
            controller.syncProgressBar.setProgress(controller.syncProgressBar.getProgress() + MainController.updateValue);
            MainController.sentFiles++;
            controller.syncText.setText(MainController.sentFiles + " из " + MainController.sendTotal + " файлов отправлено");
        }
    }
}
