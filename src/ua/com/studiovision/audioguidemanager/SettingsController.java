package ua.com.studiovision.audioguidemanager;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class SettingsController {
    public Button acceptSettings;
    public Button cancelSettings;
    public TextField rootDirectoryTextField;
    public Button setRootDirectory;
    public TextField serverApiAddressTextField;

    private Stage stage;
    private MainController mainController;
    ApplicationProperties properties = ApplicationProperties.getInstance();

    @FXML
    void initialize() {
        rootDirectoryTextField.setText(properties.getRootDirectory());
        serverApiAddressTextField.setText(properties.getServerApiAddress());
    }


    public void onSetRootDirectoryClick(ActionEvent actionEvent) {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        final File selectedDirectory = directoryChooser.showDialog(stage);
        if (selectedDirectory != null && FilenameUtils.getBaseName(selectedDirectory.getAbsolutePath()).equals("main_folder")) {
            properties.setRootDirectory(selectedDirectory.getAbsolutePath());
            rootDirectoryTextField.setText(properties.getRootDirectory());
        }
    }

    public void onAcceptSettingsButtonClick(ActionEvent actionEvent) {
        properties.setServerApiAddress(serverApiAddressTextField.getText());
        properties.save();
        mainController.setAudioDirectories();
        stage.close();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void onCancelSettingsButtonClick(ActionEvent actionEvent) {
        stage.close();
    }
}
