package ua.com.studiovision.audioguidemanager.handlers;

/**
 * @author justwatermelon
 */
public interface DisconnectHandler {
    public void disconnect();
}
