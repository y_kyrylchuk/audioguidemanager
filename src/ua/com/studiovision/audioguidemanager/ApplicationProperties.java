package ua.com.studiovision.audioguidemanager;

import java.io.*;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ApplicationProperties {
    private static final String FILE_NAME = "properties.xml";
    private static final String ROOT_DIRECTORY = "root_directory";
    private static final String SERVER_API_ADDRESS = "server_api_address";
    private static final String DEFAULT_ROOT = Paths.get(".").toAbsolutePath().normalize().toString()+"\\main_folder";
    private static File propertiesFile = new File(FILE_NAME);

    private Properties properties;
    private String rootDirectory;
    private String serverApiAddress;

    public String getRootDirectory() {
        return rootDirectory;
    }

    public String getServerApiAddress() {
        return serverApiAddress;
    }

    public void setRootDirectory(String rootDirectory) {
        this.rootDirectory = rootDirectory;
        properties.setProperty(ROOT_DIRECTORY, rootDirectory);
    }

    public void setServerApiAddress(String serverApiAddress) {
        this.serverApiAddress = serverApiAddress;
        properties.setProperty(SERVER_API_ADDRESS, serverApiAddress);
    }

    public void save() {
        try {
            OutputStream fos = new FileOutputStream(propertiesFile);
            properties.storeToXML(fos, "Application Properties");
            fos.flush();
            fos.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE,e.getMessage(),e);
        }
    }

    private ApplicationProperties() {
        properties = new Properties();
        try {
            InputStream fis = new FileInputStream(propertiesFile);
            properties.loadFromXML(fis);
            fis.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE,e.getMessage(),e);
        }

        rootDirectory = properties.getProperty(ROOT_DIRECTORY,DEFAULT_ROOT);
        serverApiAddress = properties.getProperty(SERVER_API_ADDRESS,"");
    }

    private static volatile ApplicationProperties instance;

    public static ApplicationProperties getInstance() {
        if (instance == null) {
            synchronized (ApplicationProperties.class) {
                ApplicationProperties localInstance = instance;
                if (localInstance == null) {
                    instance = new ApplicationProperties();
                    return instance;
                } else {
                    return localInstance;
                }
            }
        } else {
            return instance;
        }
    }
}
