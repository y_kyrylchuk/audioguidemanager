package ua.com.studiovision.audioguidemanager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainModel {
    private static volatile MainModel instance;
    private List<File> directories = new ArrayList<File>();

    public static MainModel getInstance() {
        if (instance == null) {
            synchronized (MainModel.class) {
                MainModel localInstance = instance;
                if (localInstance == null) {
                    instance = new MainModel();
                    return instance;
                } else {
                    return localInstance;
                }
            }
        } else {
            return instance;
        }
    }

    public List<File> getDirectories() {
        return directories;
    }
}
