package ua.com.studiovision.audioguidemanager.tree;

/**
 * @author justwatermelon
 */
public class PlaylistItem {
    PathItem pathItem;
    String playlistOutput;

    public PlaylistItem(PathItem pathItem, String playlistOutput) {
        this.pathItem = pathItem;
        this.playlistOutput = playlistOutput;
    }

    @Override
    public String toString() {
        return playlistOutput;
    }

    public PathItem getPathItem() {
        return pathItem;
    }

    public String getPlaylistOutput() {
        return playlistOutput;
    }
}
