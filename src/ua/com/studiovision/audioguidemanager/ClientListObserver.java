package ua.com.studiovision.audioguidemanager;

import ua.com.studiovision.audioguidemanager.entity.Client;
import ua.com.studiovision.audioguidemanager.entity.DeviceListItem;

import java.util.logging.Logger;

/**
 * @author justwatermelon
 */
public class ClientListObserver implements Runnable {

    private Client client;
    String boundedName;
    String status;

    public ClientListObserver(Client client, String boundedName, String status) {
        this.client = client;
        this.boundedName = boundedName;
        this.status = status;
    }

    @Override
    public void run() {
        Logger.getGlobal().info("Adding client: " + MainController.connectedDevicesModel.add(new DeviceListItem(client, boundedName, status)));
    }
}