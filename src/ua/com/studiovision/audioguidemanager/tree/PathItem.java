package ua.com.studiovision.audioguidemanager.tree;

import java.nio.file.Path;

public class PathItem {
    private Path path;
    public PathItem(Path path) {
        this.path = path;
    }

    public Path getPath() {
        return path;
    }

    @Override
    public String toString() {
        return path.getFileName().toString();
    }
}
