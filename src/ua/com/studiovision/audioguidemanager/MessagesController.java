package ua.com.studiovision.audioguidemanager;

import com.google.gson.Gson;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import ua.com.studiovision.audioguidemanager.entity.MessageListItem;
import ua.com.studiovision.audioguidemanager.entity.MessagesTemplates;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justwatermelon
 */
public class MessagesController {

    public static final String EN = "en";
    public static final String HU = "hu";
    public static final String ES = "es";
    public static final String IT = "it";
    public static final String ZH = "zh";
    public static final String DE = "de";
    public static final String NL = "nl";
    public static final String PL = "pl";
    public static final String RU = "ru";
    public static final String UA = "ua";
    public static final String FR = "fr";
    public static final String CS = "cs";
    public static final String MESSAGES_FILE = ApplicationProperties.getInstance().getRootDirectory() + "\\" + "settings" + "\\" + "messages.list";

    public ListView<MessageListItem> messagesList;
    public Button upButton;
    public Button downButton;
    public TextField enTextField;
    public TextField huTextField;
    public TextField esTextField;
    public TextField itTextField;
    public TextField zhTextField;
    public TextField deTextField;
    public TextField nlTextField;
    public TextField plTextField;
    public TextField ruTextField;
    public TextField uaTextField;
    public TextField frTextField;
    public TextField csTextField;
    public Button saveButton;
    public Button deleteButton;
    public Pane textFieldPane;
    private boolean permitCreation = true;
    private HashMap<String, TextField> fields = new HashMap<>();

    private Gson gson = new Gson();
    private ObservableList<MessageListItem> messages = FXCollections.observableArrayList();

    @FXML
    void initialize() {
        fields.put(EN, enTextField);
        fields.put(HU, huTextField);
        fields.put(ES, esTextField);
        fields.put(IT, itTextField);
        fields.put(ZH, zhTextField);
        fields.put(DE, deTextField);
        fields.put(NL, nlTextField);
        fields.put(PL, plTextField);
        fields.put(RU, ruTextField);
        fields.put(UA, uaTextField);
        fields.put(FR, frTextField);
        fields.put(CS, csTextField);
        readFromFile();
        messagesList.setItems(messages);
        messagesList.setOnMouseClicked(event -> fillMessageFields(messagesList.getSelectionModel().getSelectedItem()));
        messagesList.getParent().setOnMouseClicked(event -> {
            resetFields();
            permitCreation = true;
        });
        textFieldPane.setOnMouseClicked(event -> {});
        downButton.setGraphic(new ImageView(PlaylistController.class.getResource("/resources/down.png").toString()));
        upButton.setGraphic(new ImageView(PlaylistController.class.getResource("/resources/up.png").toString()));
        downButton.setMaxSize(25, 25);
        upButton.setMaxSize(25, 25);
    }

    public void saveToFile() {
        ArrayList<HashMap<String, String>> messages = new ArrayList<>();
        this.messages.forEach(item -> {
            HashMap<String, String> toAdd = new HashMap<>();
            item.getMessages().entrySet().forEach(entry -> {
                if (entry.getValue().length() > 0) {
                    toAdd.put(entry.getKey(), entry.getValue());
                }
            });
            if (toAdd.size() > 0) {
                messages.add(toAdd);
            }
        });
        MessagesTemplates messagesTemplates = new MessagesTemplates(messages);
        String json = gson.toJson(messagesTemplates);

        try {
            File messagesFile = new File(MESSAGES_FILE);
            PrintWriter printWriter = new PrintWriter(messagesFile,"UTF-8");
            printWriter.write(json);
            printWriter.flush();
            printWriter.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE,e.getMessage(),e);
        }
    }

    public void onSaveButtonClick(ActionEvent actionEvent) {
        int selectedIndex = messagesList.getSelectionModel().getSelectedIndex();
        MessageListItem item = new MessageListItem();
        fields.entrySet().forEach(entry -> item.getMessages().put(entry.getKey(), entry.getValue().getText()));
        if (item.getMessages().values().stream().filter(iterateItem -> iterateItem.length() > 0).findAny().isPresent()) {
            if (selectedIndex != -1 && !permitCreation) {
                messages.remove(selectedIndex);
                messages.add(selectedIndex, item);
            } else {
                messages.add(item);
            }
            messagesList.fireEvent(new ListView.EditEvent<>(messagesList, ListView.editCommitEvent(), null, -1));
            resetFields();
        }
    }

    private void fillMessageFields(MessageListItem item) {
        permitCreation = false;
        if (item != null) {
            fields.entrySet().forEach(entry -> entry.getValue().setText(item.getMessages().get(entry.getKey())));
        }
    }

    private void resetFields() {
        fields.values().forEach(TextInputControl::clear);
        messagesList.getSelectionModel().clearSelection();
    }

    private boolean readFromFile() {
        File file = new File(MESSAGES_FILE);
        if (file.getAbsoluteFile().exists()) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(MESSAGES_FILE), "UTF-8"));

                MessagesTemplates read = gson.fromJson(reader.readLine(), MessagesTemplates.class);
                read.getMessages().forEach(msg -> {
                    MessageListItem item = new MessageListItem();
                    HashMap<String, String> toRead = new HashMap<>();
                    fields.keySet().forEach(locale -> {
                        if (msg.containsKey(locale)) {
                            toRead.put(locale, msg.get(locale));
                        } else {
                            toRead.put(locale, "");
                        }
                    });
                    item.setMessages(toRead);
                    messages.add(item);
                });
                return true;
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE,e.getMessage(),e);
                return false;
            }
        } else {
            return false;
        }
    }

    public void onUpClick(ActionEvent actionEvent) {
        if (messages.size() > 1) {
            int selectedIndex = messagesList.getSelectionModel().getSelectedIndex();
            MessageListItem selectedItem = messagesList.getItems().get(selectedIndex);
            if (selectedIndex != -1 && selectedIndex != 0) {
                MessageListItem tempItem = messages.get(selectedIndex - 1);
                messages.set(selectedIndex - 1, selectedItem);
                messages.set(selectedIndex, tempItem);
                messagesList.fireEvent(new ListView.EditEvent<>(messagesList, ListView.editCommitEvent(), null, -1));
                messagesList.getSelectionModel().select(selectedIndex - 1);
            }
        }
    }

    public void onDownClick(ActionEvent actionEvent) {
        if (messages.size() > 1) {
            int selectedIndex = messagesList.getSelectionModel().getSelectedIndex();
            MessageListItem selectedItem = messagesList.getItems().get(selectedIndex);
            if (selectedIndex != -1 && selectedIndex != messages.size() - 1) {
                MessageListItem tempItem = messages.get(selectedIndex + 1);
                messages.set(selectedIndex + 1, selectedItem);
                messages.set(selectedIndex, tempItem);
                messagesList.fireEvent(new ListView.EditEvent<>(messagesList, ListView.editCommitEvent(), null, -1));
                messagesList.getSelectionModel().select(selectedIndex + 1);
            }
        }
    }

    public void onDeleteButtonClick(ActionEvent actionEvent) {
        if (messagesList.getSelectionModel().getSelectedItem() != null) {
            if (messages.size() == 1) {
                messages.clear();
            } else {
                messages.remove(messagesList.getSelectionModel().getSelectedItem());
            }
            messagesList.fireEvent(new ListView.EditEvent<>(messagesList, ListView.editCommitEvent(), null, -1));
            resetFields();
        }
    }
}
