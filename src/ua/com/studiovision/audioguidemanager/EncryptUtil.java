package ua.com.studiovision.audioguidemanager;

import org.apache.commons.io.FilenameUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justwatermelon
 */
public class EncryptUtil {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public static String publicKeyToString(PublicKey publ) throws GeneralSecurityException {
        KeyFactory fact = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec spec = fact.getKeySpec(publ,
                X509EncodedKeySpec.class);
        return base64Encode(spec.getEncoded());
    }

    public static PublicKey stringToPublicKey(String stored) throws GeneralSecurityException {
        byte[] data = base64Decode(stored);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(data);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        return fact.generatePublic(spec);
    }

    public static String[] encryptRSA(String[] dataToEncrypt, Key keyRSA) throws Exception {
        String[] encryptedData = new String[dataToEncrypt.length];
        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, keyRSA);
        for (int i = 0; i < dataToEncrypt.length; i++) {
            encryptedData[i] = base64Encode(cipher.doFinal(dataToEncrypt[i].getBytes()));
        }
        return encryptedData;
    }

    public static String[] decryptRSA(String[] dataToDecrypt, Key keyRSA) throws Exception {
        String[] decryptedData = new String[dataToDecrypt.length];
        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, keyRSA);
        for (int i = 0; i < dataToDecrypt.length; i++) {
            decryptedData[i] = new String(cipher.doFinal(base64Decode(dataToDecrypt[i]))).trim();
        }
        return decryptedData;
    }

    public static byte[] base64Decode(String base64String) {
        return Base64.getDecoder().decode(base64String);
    }

    public static String base64Encode(byte[] bytesToEncode) {
        return Base64.getEncoder().encodeToString(bytesToEncode);
    }

    public static String getMD5(String filename) throws Exception {
        byte[] b = createChecksum(filename);
        String result = "";

        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    private static byte[] createChecksum(String filename) throws Exception {
        InputStream fis = new FileInputStream(filename);

//        Files.exists(Paths.get(filename), LinkOption.NOFOLLOW_LINKS);
        
        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    public static String decryptForMD5(final String key, final String filePathToDecrypt) {
        String md5 = null;
        try {
            byte[] keyByteArray = generateKey(key);
            SecretKeySpec skeySpec = new SecretKeySpec(keyByteArray, "AES");
            Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
            byte[] ivBytes = new byte[]{0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x01};
            IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);

            BufferedInputStream in = new BufferedInputStream(new FileInputStream(filePathToDecrypt));
            File tempFile = File.createTempFile(FilenameUtils.getBaseName(filePathToDecrypt), "tmp", null);
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(tempFile));
            byte[] buff = new byte[128];
            int len;

            while ((len = in.read(buff)) > 0) {
                buff = cipher.doFinal(Arrays.copyOfRange(buff, 0, len));
                buff = Arrays.copyOfRange(buff, 0, len);
                out.write(buff, 0, len);
            }
            md5 = getMD5(tempFile.getAbsolutePath());
            in.close();
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
        return md5 != null ? md5 : "";
    }

    public static byte[] generateKey(String password) throws Exception {
        byte[] keyStart = password.getBytes("UTF-8");
        MessageDigest complete = MessageDigest.getInstance("MD5");
        complete.update(keyStart, 0, keyStart.length);
        return complete.digest();
    }

}
