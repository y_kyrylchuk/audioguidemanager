package ua.com.studiovision.audioguidemanager;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.apache.commons.io.FilenameUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justwatermelon
 */
public class EncryptService extends Service<Void> {

    private static String currentKey;

    private static final String EXTENSION = "mp3";

    @Override
    protected Task<Void> createTask() {
        return new Task<Void>() {

            @Override
            protected Void call() throws InterruptedException {
                updateProgress(-1.0, 1.0);
                try {
                    Files.walk(Paths.get(ApplicationProperties.getInstance().getRootDirectory())).forEach(filePath -> {
                        if (Files.isRegularFile(filePath) && EXTENSION.equals(FilenameUtils.getExtension(filePath.toString()))) {
                            String key = saveKeyForDirectory(filePath);
                            if (key != null)
                                encrypt(key, filePath.toString());
                            else {
                                Logger.getGlobal().warning("Current file path is null!");
                            }
                        }
                    });
                } catch (IOException e) {
                    Logger.getGlobal().severe("Error while trying to encrypt root. Details: \n" + e.getMessage());
                }
                updateProgress(1.0, 1.0);
                return null;
            }
        };
    }

    public void encrypt(final String key, final String filePathToEncrypt) {
        String filePathToSaveEncrypted = filePathToEncrypt + "s";
        File fileToSave = new File(filePathToSaveEncrypted);
        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(filePathToEncrypt));
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(fileToSave));
            byte[] keyByteArray = EncryptUtil.generateKey(key);
            SecretKeySpec skeySpec = new SecretKeySpec(keyByteArray, "AES");
            Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
            byte[] ivBytes = new byte[]{0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x01};
            IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);

            if (!Files.exists(fileToSave.toPath())) {
                fileToSave.createNewFile();
            }

            byte[] buff = new byte[128];
            int len;

            while ((len = in.read(buff)) > 0) {
                buff = cipher.doFinal(Arrays.copyOfRange(buff, 0, len));
                buff = Arrays.copyOfRange(buff, 0, len);
                out.write(buff, 0, len);
            }

            in.close();
            out.close();
            Files.deleteIfExists(Paths.get(filePathToEncrypt));
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "Encryption error:  ", e);
        }
    }

    private String saveKeyForDirectory(Path path) {
        System.out.println("PATH input: " + path.toString());
        String generated = new BigInteger(130, new SecureRandom()).toString(32);
        PrintWriter writer = null;
        try {
            File keyFile = new File(path.getParent().toString() + "\\" + "key.key");
            System.out.println("File state: " + Files.exists(keyFile.toPath(), LinkOption.NOFOLLOW_LINKS));
            if (!Files.exists(keyFile.toPath(), LinkOption.NOFOLLOW_LINKS)) {
                System.out.println("File creation status: " + keyFile.createNewFile());
                writer = new PrintWriter(path.getParent().toString() + "\\" + "key.key");
                writer.println(generated);
                writer.flush();
            } else {
                generated = readKeyFromFile(keyFile.getAbsolutePath());
            }
            System.out.println("Generated: " + generated);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null){
                writer.close();
            }
        }
        return generated;
    }

    private String readKeyFromFile(String keyFile) {
        String key = null;
        File file = new File(keyFile);
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
            key = bufferedReader.readLine();
            bufferedReader.close();
        } catch (IOException e) {
            Logger.getGlobal().severe("Error while reading encryption key: " + e.getMessage());
        }
        return key;
    }
}
