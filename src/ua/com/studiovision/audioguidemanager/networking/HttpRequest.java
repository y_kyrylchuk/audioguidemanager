package ua.com.studiovision.audioguidemanager.networking;

import java.util.HashSet;

/**
 * @author justwatermelon
 */
public class HttpRequest {
    String action;
    Data data;

    public HttpRequest(String action, Data data) {
        this.action = action;
        this.data = data;
    }

    static class Data{
        String imei;
        String time_from;
        String time_to;
        String timeZone;
        HashSet<String> dates;

        public Data(String imei, String time_from, String time_to, String timeZone, HashSet<String>dates) {
            this.imei = imei;
            this.time_from = time_from;
            this.time_to = time_to;
            this.timeZone = timeZone;
            this.dates = dates;
        }
    }
}
