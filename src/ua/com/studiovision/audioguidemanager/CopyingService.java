package ua.com.studiovision.audioguidemanager;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.apache.commons.io.FilenameUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.*;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justwatermelon
 */
public class CopyingService extends Service<Void> {
    public static final String MP3S_EXTENSION = ".mp3s";
    private File source;
    private File destination;
    private long total;
    private long workDone;
    private DecimalFormat decimalFormat = new DecimalFormat("00.00");


    public CopyingService(File source, File destination, long total) {
        this.source = source;
        this.destination = destination;
        this.total = total;
    }

    @Override
    protected Task<Void> createTask() {
        return new Task<Void>() {

            @Override
            protected Void call() throws InterruptedException {
                updateProgress(0.0, 1.0);
                System.out.println(source.getPath() + " " + destination.getPath());
                copyFolder(source, destination);
                updateProgress(1.0, 1.0);
                return null;
            }

            public void copyFolder(File src, File destination) {
                if (src.isDirectory()) {

                    if (!destination.isDirectory()) {
                        destination.mkdir();
                    }

                    String files[] = src.list();

                    for (String file : files) {
                        File srcFile = new File(src, file);
                        File destinationFile = new File(destination, file);
                        copyFolder(srcFile, destinationFile);
                    }
                } else {
                    try {
                        String basePath = FilenameUtils.getFullPath(destination.getAbsolutePath());
                        String possibleFilePath = basePath.substring(basePath.indexOf("main_folder"))
                                + FilenameUtils.getBaseName(destination.getAbsolutePath()) + MP3S_EXTENSION;
                        Logger.getGlobal().info(possibleFilePath);
                        File possibleFile = new File(possibleFilePath);
                        if (Files.exists(possibleFile.toPath(), LinkOption.NOFOLLOW_LINKS)) {
                            if (possibleFile.getAbsolutePath().contains("mp3s")) {
                                String md5Possible = EncryptUtil.getMD5(possibleFilePath);
                                boolean equals = checkMD5Equals(md5Possible,readKeyFromFile(basePath.substring(basePath.indexOf("main_folder")) + "key.key"), src.getAbsolutePath(),possibleFilePath);
                                if (equals) {
                                    Logger.getGlobal().info("Files are equals. Breaking rewriting...");
                                    workDone += possibleFile.length();
                                    updateProgress(workDone / (double) total, 1.0);
                                    updateMessage("Скопировано " + decimalFormat.format((workDone / (double) total) * 100) + " %");
                                }
                                return;
                            } else {
                                Logger.getGlobal().info("File exists");
                                if (EncryptUtil.getMD5(src.getAbsolutePath()).equals(EncryptUtil.getMD5(destination.getAbsolutePath()))) {
                                    Logger.getGlobal().info("Files are equals. Breaking rewriting...");
                                    workDone += possibleFile.length();
                                    updateProgress(workDone / (double) total, 1.0);
                                    updateMessage("Скопировано " + decimalFormat.format((workDone / (double) total) * 100) + " %");
                                    return;
                                } else {
                                    Logger.getGlobal().info("Delete non-relevant file: " + possibleFile.delete());
                                }
                            }
                        } else {
                            Logger.getGlobal().info(possibleFile.getAbsolutePath() + " not exists");
                        }

                        InputStream in = new FileInputStream(src);
                        OutputStream out = new FileOutputStream(destination);
                        byte[] buffer = new byte[1024];
                        int length;
                        while ((length = in.read(buffer)) > 0) {
                            workDone += length;
                            updateProgress(workDone / (double) total, 1.0);
                            updateMessage("Скопировано " + decimalFormat.format((workDone / (double) total) * 100) + " %");
                            out.write(buffer, 0, length);
                        }
                        in.close();
                        out.flush();
                        out.close();
                    } catch (Exception e) {
                        Logger.getGlobal().log(Level.SEVERE, "Error while copying files...", e);
                    }
                }
            }

            private String readKeyFromFile(String keyFile) {
                String key = null;
                File file = new File(keyFile);
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
                    key = bufferedReader.readLine();
                    bufferedReader.close();
                } catch (IOException e) {
                    Logger.getGlobal().severe("Error while reading encryption key: " + e.getMessage());
                }
                return key;
            }

            public boolean checkMD5Equals(final String md5Possible, final String key, final String filePathToEncrypt, final String possibleFilePath) {
                String filePathToSaveEncrypted = possibleFilePath + "Test";
                File fileToSave = new File(filePathToSaveEncrypted);
                try {
                    BufferedInputStream in = new BufferedInputStream(new FileInputStream(filePathToEncrypt));
                    BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(fileToSave));
                    byte[] keyByteArray = EncryptUtil.generateKey(key);
                    SecretKeySpec skeySpec = new SecretKeySpec(keyByteArray, "AES");
                    Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
                    byte[] ivBytes = new byte[]{0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x01};
                    IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
                    cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);

                    byte[] buff = new byte[128];
                    int len;

                    while ((len = in.read(buff)) > 0) {
                        buff = cipher.doFinal(Arrays.copyOfRange(buff, 0, len));
                        buff = Arrays.copyOfRange(buff, 0, len);
                        out.write(buff, 0, len);
                    }

                    in.close();
                    out.close();

                    String md5Origin = EncryptUtil.getMD5(filePathToSaveEncrypted);
                    System.out.println(md5Origin + " = " + md5Possible);
                    if (!md5Origin.equals(md5Possible)){
                        Files.delete(Paths.get(possibleFilePath));
                        InputStream inStream = new FileInputStream(filePathToSaveEncrypted);
                        OutputStream outStream = new FileOutputStream(possibleFilePath);
                        byte[] buffer = new byte[1024];
                        int length;
                        while ((length = inStream.read(buffer)) > 0) {
                            workDone += length;
                            updateProgress(workDone / (double) total, 1.0);
                            updateMessage("Скопировано " + decimalFormat.format((workDone / (double) total) * 100) + " %");
                            outStream.write(buffer, 0, length);
                        }
                        inStream.close();
                        outStream.flush();
                        outStream.close();
                    }
                    fileToSave.delete();
                    return true;
                } catch (Exception e) {
                    Logger.getGlobal().log(Level.SEVERE, "Encryption error:  ", e);
                }
                return false;
            }
        };
    }
}