package ua.com.studiovision.audioguidemanager.tree;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Comparator;

public class PathTreeItem extends TreeItem<PathItem> {
    private boolean isLeaf = false;
    private boolean isFirstTimeChildren = true;
    private boolean isFirstTimeLeft = true;
    private boolean dirBuilder = false;

    private PathTreeItem(PathItem pathItem, boolean dirBuilder) {
        super(pathItem);
        this.dirBuilder = dirBuilder;
    }

    public static TreeItem<PathItem> createNode(PathItem pathItem, boolean dirBuilder) {
        return new PathTreeItem(pathItem, dirBuilder);
    }

    @Override
    public ObservableList<TreeItem<PathItem>> getChildren() {
        if (isFirstTimeChildren) {
            isFirstTimeChildren = false;
            super.getChildren().setAll(buildChildren(this));
        }
        return super.getChildren();
    }

    @Override
    public boolean isLeaf() {
        if (isFirstTimeLeft) {
            isFirstTimeLeft = false;
            Path path = getValue().getPath();
            if (dirBuilder){
                File node = path.toFile();
                File[] list = node.listFiles();
                if (list != null) {
                    for (int i = 0; i < list.length; i++) {
                        if ("key".equals(FilenameUtils.getExtension(list[i].getAbsolutePath()))) {
                            isLeaf = true;
                        }
                    }
                }
            } else {
                isLeaf = !Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS);
            }
        }
        return isLeaf;
    }

    private ObservableList<TreeItem<PathItem>> buildChildren(TreeItem<PathItem> treeItem) {
        Path path = treeItem.getValue().getPath();
        if (path != null && Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
            ObservableList<TreeItem<PathItem>> children = FXCollections.observableArrayList();
            try (DirectoryStream<Path> dirs = Files.newDirectoryStream(path)) {
                for (Path dir : dirs) {
                    PathItem pathItem = new PathItem(dir);
                    if (dirBuilder) {
                        if (!dir.getFileName().toString().contains(".")) {
                            children.add(createNode(pathItem, true));
                        }
                    } else {
                        children.add(createNode(pathItem, false));
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            children.sort(Comparator.comparing(t->t.getValue().getPath().getFileName()));
            return children;
        }
        return FXCollections.emptyObservableList();
    }
}
