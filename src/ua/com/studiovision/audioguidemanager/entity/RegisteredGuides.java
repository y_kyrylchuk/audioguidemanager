package ua.com.studiovision.audioguidemanager.entity;

import java.util.HashMap;

/**
 * @author justwatermelon
 */
public class RegisteredGuides {
    HashMap<String,GuideListItem> registeredGuides;

    public RegisteredGuides(HashMap<String, GuideListItem> registeredGuides) {
        this.registeredGuides = registeredGuides;
    }

    public HashMap<String, GuideListItem> getRegisteredGuides() {
        return registeredGuides;
    }
}
