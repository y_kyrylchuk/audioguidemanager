package ua.com.studiovision.audioguidemanager;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AudioGuideManager extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MainLayout.fxml"));
        Parent root = fxmlLoader.load();
        MainController controller = fxmlLoader.getController();
        primaryStage.setScene(new Scene(root));
        controller.setStage(primaryStage);
        controller.startNetworking(9000);
        primaryStage.getIcons().add(new Image(AudioGuideManager.class.getResource("/resources/"+"icon.png").toString()));
        primaryStage.setTitle("Аудиогид менеджер");
        primaryStage.initStyle(StageStyle.DECORATED);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
