package ua.com.studiovision.audioguidemanager.networking;

import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import ua.com.studiovision.audioguidemanager.MainController;
import ua.com.studiovision.audioguidemanager.entity.Client;
import ua.com.studiovision.audioguidemanager.entity.DeviceListItem;
import ua.com.studiovision.audioguidemanager.entity.SavedNames;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justwatermelon
 */
public class Server implements Runnable {
    protected int serverPort = 9000;
    public static int clientCounter = 0;
    public static int guideCounter = 0;
    protected ServerSocket serverSocket = null;
    protected boolean isStopped = false;
    protected Thread runningThread = null;
    private BroadcastMessageThread broadcastMessageThread;
    public static ConcurrentHashMap<String, Client> clients = new ConcurrentHashMap<>();
    public static KeyPair keyPair = generateRSAKeyPair();
    public static SavedNames names;
    public static SavedNames guideNames;
    public static volatile int audioListResponses = 0;

    public Server(int port) {
        this.serverPort = port;
        broadcastMessageThread = new BroadcastMessageThread(9001);
        String savedTourists = loadSavedNames(true);
        if (savedTourists != null) {
            names = new Gson().fromJson(savedTourists, SavedNames.class);
        } else {
            names = new SavedNames(new HashMap<>());
        }
        String savedGuides = loadSavedNames(false);
        if (savedGuides != null) {
            guideNames = new Gson().fromJson(savedGuides, SavedNames.class);
        } else {
            guideNames = new SavedNames(new HashMap<>());
        }
        refreshNamesCount();
        guideNames.getNames().forEach((imei,name)-> MainController.connectedDevicesModel.add(new DeviceListItem(null,name,"Disconnected")));
        names.getNames().forEach((imei,name)-> MainController.connectedDevicesModel.add(new DeviceListItem(null,name,"Disconnected")));
        MainController.connectedDevicesModel.sort(Comparator.comparing(DeviceListItem::getBoundedName));
    }

    public void run() {
        Logger.getGlobal().info("Server started...");
        synchronized (this) {
            this.runningThread = Thread.currentThread();
        }
        openServerSocket();
        new Thread(broadcastMessageThread).start();
        while (!isStopped()) {
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
            } catch (IOException e) {
                if (isStopped()) {
                    saveNames();
                    Logger.getGlobal().info("Server stopped.");
                    return;
                }
                throw new RuntimeException("Error accepting client connection", e);
            }
            String key = clientSocket.getInetAddress().getHostAddress();
            if (clients.containsKey(key)) {
                Logger.getGlobal().info("Refreshing connection for " + key);
                clients.get(key).workerRunnable.stop();
                clients.remove(key);
            }
            WorkerRunnable workerRunnable = new WorkerRunnable(clientSocket);
            new Thread(workerRunnable).start();
            clients.put(clientSocket.getInetAddress().getHostAddress(), new Client(workerRunnable));
        }
        saveNames();
        Logger.getGlobal().info("Server stopped.");
    }


    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop() {
        this.isStopped = true;
        try {
            this.serverSocket.close();
            this.broadcastMessageThread.stop();
            clients.forEach((imei, client) -> client.workerRunnable.stop());
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port " + this.serverPort, e);
        }
    }

    private static KeyPair generateRSAKeyPair() {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(1024);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return keyPairGenerator.genKeyPair();
    }

    private static String loadSavedNames(boolean isTourists) {
        File file;
        if (isTourists) {
            file = new File("saved_tourists.agm");
        } else {
            file = new File("saved_guides.agm");
        }
        FileInputStream fis;
        String result = null;
        if (file.getAbsoluteFile().exists()) {
            try {
                fis = new FileInputStream(file);
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                fis.close();
                result = new String(data, "UTF-8");
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE,"IOException while loading names...",e);
            }
            Logger.getGlobal().info("Names loaded...");
        }
        return result;
    }

    public static void refreshNamesCount(){
        clientCounter = names.getNames().size();
        guideCounter = guideNames.getNames().size();
    }

    public static void saveNames() {
        String toSaveTourists = new Gson().toJson(names);
        String toSaveGuides = new Gson().toJson(guideNames);
        File touristsFile = new File("saved_tourists.agm");
        File guidesFile = new File("saved_guides.agm");
        if (!touristsFile.getAbsoluteFile().exists()) {
            try {
                Logger.getGlobal().info("Tourists file creation status: " + touristsFile.createNewFile());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!guidesFile.getAbsoluteFile().exists()) {
            try {
                Logger.getGlobal().info("Guides file creation status: " + guidesFile.createNewFile());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            IOUtils.write(toSaveTourists.getBytes(), new FileOutputStream(touristsFile));
            IOUtils.write(toSaveGuides.getBytes(), new FileOutputStream(guidesFile));
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, "IOException while saving names...",e);
        }
        Logger.getGlobal().info("Names saved...");
    }
}