package ua.com.studiovision.audioguidemanager.requests;

/**
 * @author justwatermelon
 */
public class SyncResponse{
    String path;
    byte[] audioFile;

    public SyncResponse(String path, byte[] audioFile) {
        this.path = path;
        this.audioFile = audioFile;
    }

    public byte[] getAudioFile() {
        return audioFile;
    }

    public String getPath() {
        return path;
    }
}