package ua.com.studiovision.audioguidemanager;

import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * @author justwatermelon
 */
public class HelpController {
    public WebView webView;

    @FXML
    void initialize(){
        WebEngine engine = webView.getEngine();
        String url = HelpController.class.getResource("/resources/help/Audio Guide Manager.html").toExternalForm();
        engine.load(url);
    }
}
