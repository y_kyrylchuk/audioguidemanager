package ua.com.studiovision.audioguidemanager.networking;

/**
 * @author justwatermelon
 */
public class ServerResponse {
    String status;
    String message;

    public ServerResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
