package ua.com.studiovision.audioguidemanager.networking;

import com.google.gson.Gson;
import javafx.application.Platform;
import org.apache.commons.io.FilenameUtils;
import ua.com.studiovision.audioguidemanager.ApplicationProperties;
import ua.com.studiovision.audioguidemanager.ClientListObserver;
import ua.com.studiovision.audioguidemanager.EncryptUtil;
import ua.com.studiovision.audioguidemanager.MainController;
import ua.com.studiovision.audioguidemanager.entity.Client;
import ua.com.studiovision.audioguidemanager.entity.DeviceListItem;
import ua.com.studiovision.audioguidemanager.entity.SavedNames;
import ua.com.studiovision.audioguidemanager.requests.ClientInfo;
import ua.com.studiovision.audioguidemanager.requests.ServerInfo;
import ua.com.studiovision.audioguidemanager.requests.SyncRequest;

import java.io.*;
import java.net.Socket;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justwatermelon
 */
public class WorkerRunnable implements Runnable {
    public static final int REQUEST_TIMEOUT = 60000;
    private Socket clientSocket = null;
    private static final Gson gson = new Gson();
    private int workerSentFile = 0;
    private int workerToSendFiles = 0;
    private Optional<DeviceListItem> boundedDeviceListItem;
    public boolean syncInProgress = false;
    private Thread thread = Thread.currentThread();
    public static final Object lock = new Object();

    public Thread timeoutThread = threadCreate(REQUEST_TIMEOUT);

    public WorkerRunnable(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public Thread threadCreate(int timeout) {
        return new Thread(() -> {
            try {
                Logger.getGlobal().info("Timeout thread (" + timeout / 1000 + " s) started!");
                Thread.sleep(timeout);
                Logger.getGlobal().info("Timeout fired!");
                stop();
            } catch (InterruptedException e) {
                Logger.getGlobal().log(Level.SEVERE, "Timeout interrupted...");
            }
        });
    }

    public Thread getThread() {
        return thread;
    }

    public void run() {
        try {
            Logger.getGlobal().info("Start listening for client requests:");
            DataInputStream input = new DataInputStream(clientSocket.getInputStream());
            while (!Thread.currentThread().isInterrupted()) {
                if (input.available() > 0) {
                    int suggestedCode;
                    if ((suggestedCode = input.readByte()) < 8 && suggestedCode >= 0) {
                        Logger.getGlobal().info("Got client request. Available: " + input.available() + ". Request code: " + suggestedCode);
                        processRequest(suggestedCode, input.readUTF());
                        Logger.getGlobal().info("Request processed successfully.");
                    }
                }
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            input.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, "Error while reading request...", e);
            stop();
        }
    }

    private void processRequest(int code, String json) throws IOException {
        String response = null;
        Client client = Server.clients.get(this.clientSocket.getInetAddress().getHostAddress());
        if (timeoutThread.getState().equals(Thread.State.TIMED_WAITING)) {
            timeoutThread.interrupt();
            timeoutThread = threadCreate(REQUEST_TIMEOUT);
        }
        switch (code) {
            case NetworkProtocol.CLIENT_KEY:
                //Extract pub-key and set it to client
                Server.clients.get(this.clientSocket.getInetAddress().getHostAddress()).setKey(json);
                //Sending own pub-key to client
                try {
                    response = EncryptUtil.publicKeyToString(Server.keyPair.getPublic());
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                }
                sendMessage(NetworkProtocol.SERVER_KEY, response);
                break;
            case NetworkProtocol.CLIENT_INFO:
                //Extract client info and set it to client
                ClientInfo clientInfo = gson.fromJson(json, ClientInfo.class);
                String[] toDecode = {clientInfo.getImei()};
                String[] decryptedImei = null;
                try {
                    decryptedImei = EncryptUtil.decryptRSA(toDecode, Server.keyPair.getPrivate());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                client.setImei(decryptedImei[0]);
                client.setType(clientInfo.getType());

                if (client.getType().equals("tourist")) {
                    setName(Server.names, client);
                } else if (client.getType().equals("guide")) {
                    setName(Server.guideNames, client);
                }
                Server.saveNames();
                Server.refreshNamesCount();

                Logger.getGlobal().info(">>>>>>>>CHANGE STATUS");
                //if exists in the shown list -> change status
                boolean presenceFlag;
                Optional<DeviceListItem> result = MainController.connectedDevicesModel.stream().filter(deviceListItem -> deviceListItem.getBoundedName().equals(client.getName())).findFirst();
                boundedDeviceListItem = result;
                if (result.isPresent()) {
                    result.get().setClient(client);
                    result.get().setStatus("Connected");
                    presenceFlag = true;
                    MainController.updateHandler.refreshDevices();
                } else {
                    presenceFlag = false;
                }

                if (!presenceFlag) {
                    Platform.runLater(new ClientListObserver(client, client.getName(), "Connected"));
                }

                if (clientInfo.isRegister()) {
                    Logger.getGlobal().info("Register client: " + client.getName());
                    //Send attached name to client
                    String[] toEncode = {client.getImei()};
                    String[] encodedImei = null;
                    try {
                        encodedImei = EncryptUtil.encryptRSA(toEncode, EncryptUtil.stringToPublicKey(client.getKey()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    response = gson.toJson(new ServerInfo(encodedImei[0], client.getName()));
                    sendMessage(NetworkProtocol.SERVER_INFO, response);
                } else {
                    Logger.getGlobal().info("Client connected: " + client.getName());
                }
                break;
            case NetworkProtocol.SYNC_REQUEST:
                //extract client audio list
                client.setAudios(gson.fromJson(json, SyncRequest.class).getAudios());
                Logger.getGlobal().info("Client audios: " + client.getAudios().size());
                MainController.updateHandler.incrementAudioLists();
                if (Server.audioListResponses == Server.clients.size()) {
                    MainController.updateHandler.hide();
                    MainController.updateHandler.showButtons();
                }
                break;
            case NetworkProtocol.RECEIVE_CONFIRM:
                if (workerSentFile != MainController.responseHashMap.get(client).size()) {
                    syncInProgress = true;
                    Path path = MainController.responseHashMap.get(client).get(workerSentFile);
                    byte[] fileToTransfer;
                    String convertedPath = path.toString().replace("\\", "/");
                    if ("key".equals(FilenameUtils.getExtension(path.getFileName().toString()))) {
                        fileToTransfer = encryptKeyForClient(client, path.toString());
                    } else {
                        fileToTransfer = readFileBytes(path.toString());
                    }
                    Logger.getGlobal().info(MainController.responseHashMap.get(client).get(workerSentFile).toString());
                    sendBytes(fileToTransfer, convertedPath);
                    workerSentFile++;
                    if (boundedDeviceListItem.isPresent()) {
                        boundedDeviceListItem.get().setStatus("Sync " + workerSentFile + " ... " + MainController.responseHashMap.get(client).size());
                    }
                    MainController.updateHandler.refreshDevices();
                    MainController.updateHandler.update();
                } else {
                    syncInProgress = false;
                    clientSocket.getOutputStream().close();
                    clientSocket.getInputStream().close();
                }
                break;
        }
    }

    private void setName(SavedNames names, Client client) {
        if (names.getNames().containsKey(client.getImei())) {
            client.setName(names.getNames().get(client.getImei()));
        } else {
            client.setName(generateNextClientName(client));
            names.getNames().put(client.getImei(), client.getName());
        }
    }

    public void sendMessage(int code, String message) throws IOException {
        Logger.getGlobal().info(">>>Sending message on code: " + code);
        Logger.getGlobal().info("Message length: " + message.getBytes().length);
        DataOutputStream os = new DataOutputStream(clientSocket.getOutputStream());
        os.writeByte(code);
        os.writeUTF(message);
        os.flush();
    }

    public void sendBytes(byte[] myByteArray, String fileName) throws IOException {
        Logger.getGlobal().info("File length: " + myByteArray.length);
        sendBytes(myByteArray, 0, myByteArray.length, fileName);
    }

    public void sendBytes(byte[] myByteArray, int start, int len, String fileName) throws IOException {
        if (len < 0)
            throw new IllegalArgumentException("Negative length not allowed");
        if (start < 0 || start >= myByteArray.length)
            throw new IndexOutOfBoundsException("Out of bounds: " + start);
        OutputStream out = clientSocket.getOutputStream();
        DataOutputStream dos = new DataOutputStream(out);

        dos.writeByte(NetworkProtocol.SYNC_RESPONSE);
        int timeout = (int) (Math.ceil(((double) len / (1024 * 1024)) / 50)) * REQUEST_TIMEOUT;
        Logger.getGlobal().info("Timeout: " + timeout);
        dos.writeInt(timeout);
        dos.writeInt(len);
        dos.writeUTF(fileName);
        if (len > 0) {
            dos.write(myByteArray);
        }
        dos.flush();
        if (!timeoutThread.getState().equals(Thread.State.NEW)) {
            timeoutThread = threadCreate(timeout);
        }
        timeoutThread.start();
        Logger.getGlobal().info("Transfer end");
    }

    public void stop() {
        Thread.currentThread().interrupt();
        timeoutThread.interrupt();
        Client client = Server.clients.get(clientSocket.getInetAddress().getHostAddress());
        try {
            if (client != null) {
                Logger.getGlobal().info("Client disconnected: " + client.getName());
                Optional<DeviceListItem> result = MainController.connectedDevicesModel.stream().filter(device -> device.getBoundedName().equals(client.getName())).findFirst();
                if (syncInProgress) {
                    syncInProgress = false;
                    int sendDifference = MainController.responseHashMap.get(client).size() - workerSentFile;
                    for (int i = 0; i < sendDifference; i++) {
                        workerSentFile++;
                        MainController.updateHandler.update();
                    }
                    MainController.updateHandler.progressBarRepaint();
                    MainController.failureSyncClients.add(client.getName());
                    if (result.isPresent())
                        result.get().setStatus("Disconnected by timeout");
                    synchronized (lock) {
                        lock.notifyAll();
                    }
                } else {
                    if (result.isPresent())
                        result.get().setStatus("Disconnected");
                    synchronized (lock) {
                        lock.notifyAll();
                    }
                }
                if (result.isPresent())
                    result.get().setClient(null);
                MainController.updateHandler.refreshDevices();
                if (MainController.getSentFiles() == MainController.sendTotal) {
                    syncInProgress = false;
                    workerSentFile = 0;
                    if (MainController.failureSyncClients.size() > 0) {
                        MainController.updateHandler.showFailureTransferClients();
                    }
                    MainController.updateHandler.hide();
                    MainController.updateHandler.showButtons();
                }
                try {
                    clientSocket.shutdownOutput();
                    clientSocket.shutdownInput();
                    clientSocket.close();
                    Server.clients.remove(clientSocket.getInetAddress().getHostAddress());
                } catch (IOException e) {
                    Logger.getGlobal().log(Level.SEVERE, "Disconnecting exception");
                }
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    private byte[] readFileBytes(String path) {
        byte[] byteFile = null;
        System.out.println("File:" + path);
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(ApplicationProperties.getInstance().getRootDirectory() + path));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int read;
            while ((read = bis.read(buffer, 0, buffer.length)) != -1) {
                baos.write(buffer, 0, read);
            }
            byteFile = baos.toByteArray();
            baos.flush();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return byteFile;
    }

    private byte[] encryptKeyForClient(Client client, String path) {
        byte[] keyFile;
        keyFile = readFileBytes(path);
        String[] toEncrypt = {new String(keyFile)};
        String[] encrypted = null;
        try {
            encrypted = EncryptUtil.encryptRSA(toEncrypt, EncryptUtil.stringToPublicKey(client.getKey()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return encrypted[0].getBytes();
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public int getWorkerSentFile() {
        return workerSentFile;
    }

    public static Gson getGson() {
        return gson;
    }

    public Thread getTimeoutThread() {
        return timeoutThread;
    }

    public void setTimeoutThread(Thread timeoutThread) {
        this.timeoutThread = timeoutThread;
    }

    private String generateNextClientName(Client client) {
        if (client.getType().equals("tourist")) {
            Server.clientCounter++;
            return "Tourist #" + Server.clientCounter;
        } else {
            Server.guideCounter++;
            return "Guide #" + Server.guideCounter;
        }
    }
}