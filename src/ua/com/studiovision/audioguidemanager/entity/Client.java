package ua.com.studiovision.audioguidemanager.entity;

import ua.com.studiovision.audioguidemanager.networking.WorkerRunnable;

import java.util.HashMap;

/**
 * @author justwatermelon
 */
public class Client {
    String name;
    String key;
    String type;
    String imei;
    HashMap<String,String> audios;
    public WorkerRunnable workerRunnable;

    public Client(WorkerRunnable workerRunnable) {
        this.workerRunnable = workerRunnable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public HashMap<String, String> getAudios() {
        return audios;
    }

    public void setAudios(HashMap<String, String> audios) {
        this.audios = audios;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Client name: ")
                .append(name)
                .append(", key: ")
                .append(key)
                .append(", type: ")
                .append(type)
                .append(", imei: ")
                .append(imei);
        return builder.toString();
    }
}
