package ua.com.studiovision.audioguidemanager.entity;

import java.util.HashSet;

/**
 * @author justwatermelon
 */
public class GuideListItem {
    private String name;
    private String imei;
    private String timeFrom;
    private String timeTo;
    private String timezone;
    boolean register;
    HashSet<String> calendars;

    public GuideListItem(String name, String imei, String timeFrom, String timeTo, String timezone, boolean register) {
        this.name = name;
        this.imei = imei;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.timezone = timezone;
        this.register = register;
        this.calendars = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public boolean isRegister() {
        return register;
    }

    public void setRegister(boolean register) {
        this.register = register;
    }

    public HashSet<String> getCalendars() {
        return calendars;
    }

    public void setCalendars(HashSet<String> calendars) {
        this.calendars = calendars;
    }

    @Override
    public String toString() {
        return name;
    }
}