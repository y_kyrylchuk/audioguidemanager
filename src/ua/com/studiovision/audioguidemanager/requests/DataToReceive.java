package ua.com.studiovision.audioguidemanager.requests;

/**
 * @author justwatermelon
 */
public class DataToReceive {
    int count;
    long totalSize;

    public DataToReceive(int count, long totalSize) {
        this.count = count;
        this.totalSize = totalSize;
    }
}
