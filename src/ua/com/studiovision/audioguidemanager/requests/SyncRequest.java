package ua.com.studiovision.audioguidemanager.requests;

import java.util.HashMap;

/**
 * @author justwatermelon
 */
public class SyncRequest{
    HashMap<String,String> audios;

    public SyncRequest(HashMap<String, String> audios) {
        this.audios = audios;
    }

    public HashMap<String, String> getAudios() {
        return audios;
    }
}
