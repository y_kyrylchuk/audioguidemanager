package ua.com.studiovision.audioguidemanager.entity;

import ua.com.studiovision.audioguidemanager.MessagesController;

import java.util.HashMap;

/**
 * @author justwatermelon
 */
public class MessageListItem {
    String defaultLocale = MessagesController.RU;
    HashMap<String, String> messages = new HashMap<>();

    public String getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(String defaultLocale) {
        this.defaultLocale = defaultLocale;
    }

    public HashMap<String, String> getMessages() {
        return messages;
    }

    public void setMessages(HashMap<String, String> messages) {
        this.messages = messages;
    }

    @Override
    public String toString() {
        if (!messages.get(defaultLocale).equals("")){
            return messages.get(defaultLocale);
        } else {
            return messages.values().stream().filter(item -> item.length() > 0).findFirst().get();
        }
    }
}
