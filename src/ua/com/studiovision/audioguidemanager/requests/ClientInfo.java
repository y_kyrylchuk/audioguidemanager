package ua.com.studiovision.audioguidemanager.requests;

/**
 * @author justwatermelon
 */
public class ClientInfo{
    String type;
    String imei;
    boolean register;

    public ClientInfo(String type, String imei, boolean register) {
        this.type = type;
        this.imei = imei;
        this.register = register;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public boolean isRegister() {
        return register;
    }

    public void setRegister(boolean register) {
        this.register = register;
    }
}
