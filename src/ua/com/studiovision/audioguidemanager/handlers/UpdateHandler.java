package ua.com.studiovision.audioguidemanager.handlers;

/**
 * @author justwatermelon
 */
public interface UpdateHandler {
    public void update();
    public void hide();
    public void incrementAudioLists();
    public void showButtons();
    public void refreshDevices();
    public void refreshServerAudio();
    public void progressBarRepaint();
    public void showFailureTransferClients();
}