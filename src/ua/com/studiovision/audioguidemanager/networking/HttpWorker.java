package ua.com.studiovision.audioguidemanager.networking;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.controlsfx.dialog.Dialogs;
import ua.com.studiovision.audioguidemanager.ApplicationProperties;
import ua.com.studiovision.audioguidemanager.entity.GuideListItem;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author justwatermelon
 */
public class HttpWorker {
    private static final String ACTION_REGISTER = "users.guide-register";
    private static final String ACTION_CHANGE_TIME = "users.change-time-work-guide";

    @SuppressWarnings("deprecation")
    public static String[] performRequest(GuideListItem item) {

        StringBuilder requestBuilder = new StringBuilder();
        if (item.isRegister()) {
            requestBuilder.append(new Gson().toJson(new HttpRequest(ACTION_REGISTER, new HttpRequest.Data(item.getImei(), item.getTimeFrom(), item.getTimeTo(), item.getTimezone(), item.getCalendars()))));
            Logger.getGlobal().info("Register action");
        } else {
            requestBuilder.append(new Gson().toJson(new HttpRequest(ACTION_CHANGE_TIME, new HttpRequest.Data(item.getImei(), item.getTimeFrom(), item.getTimeTo(), item.getTimezone(), item.getCalendars()))));
            Logger.getGlobal().info("Change action");
        }
        ServerResponse serverResponse = null;
        try {
            String server_url = ApplicationProperties.getInstance().getServerApiAddress();
            if (server_url!=null && !server_url.equals("")) {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost request = new HttpPost(server_url);
                List<NameValuePair> urlParameters = new ArrayList<>();
                urlParameters.add(new BasicNameValuePair("data", requestBuilder.toString()));
                Logger.getGlobal().info("Request: " + Arrays.toString(urlParameters.toArray()));
                request.setEntity(new UrlEncodedFormEntity(urlParameters));
                HttpResponse response = httpClient.execute(request);

                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuilder result = new StringBuilder();
                String line;
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }
                Logger.getGlobal().info("Response<<<" + result.toString());
                serverResponse = new Gson().fromJson(result.toString(), ServerResponse.class);
            } else {
                Dialogs dialog = Dialogs.create().message("Введите адрес API сервера").title("Внимание!");
                dialog.showWarning();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return recognizeMessage(serverResponse.getMessage());
    }

    private static String[] recognizeMessage(String httpResponse){
        String recognizedMessage;
        int code;
        switch (httpResponse){
            case "guide add success":
                recognizedMessage = "Гид успешно зарегистрирован";
                code = 0;
                break;
            case "update time success":
                recognizedMessage = "Время успешно изменено";
                code = 1;
                break;
            case "time not H:i:s":
                recognizedMessage = "Неверный формат времени";
                code = 2;
                break;
            case "imei not numeric":
                recognizedMessage = "Такого гида не существует";
                code = 3;
                break;
            case "this imei alredy use":
                recognizedMessage = "Этот гид уже зрегистрирован. Пожалуйста, выполните запрос на изменение";
                code = 4;
                break;
            default:
                recognizedMessage = "Ошибка запроса к серверу";
                code = 5;
                break;
        }
        return new String[] {String.valueOf(code),recognizedMessage};
    }
}