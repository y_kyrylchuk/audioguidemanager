package ua.com.studiovision.audioguidemanager.entity;

/**
 * @author justwatermelon
 */
public class DeviceListItem {
    private Client client;
    private String boundedName;
    private String status;

    public DeviceListItem(Client client, String boundedName, String status) {
        this.client = client;
        this.boundedName = boundedName;
        this.status = status;
    }

    @Override
    public String toString() {
        if (client != null) {
            return client.name + "   " + status;
        }
        return boundedName + "   " + status;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getBoundedName() {
        return boundedName;
    }

    public void setBoundedName(String boundedName) {
        this.boundedName = boundedName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}