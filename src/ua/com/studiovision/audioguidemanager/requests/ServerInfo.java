package ua.com.studiovision.audioguidemanager.requests;

/**
 * @author justwatermelon
 */
public class ServerInfo{
    String imei;
    String name;

    public ServerInfo(String imei, String name) {
        this.imei = imei;
        this.name = name;
    }
}